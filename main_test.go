package main

import (
	"encoding/json"
	"testing"
)

func Test_makeBaseURL(t *testing.T) {
	testurl := "/projectinfo/v1/github.com/apache/kafka"
	url := makeBaseURL(testurl)
	if url != "https://api.github.com/repos/apache/kafka" {
		t.Error("wrong url")
	}
	testurl = "/projectinfo/v1/github/ap"
	url = makeBaseURL(testurl)
	if url != "" {
		t.Error("expected empty string")
	}
}

func Test_makeFP(t *testing.T) {
	var realFP fullProject
	var P project
	var C []contributor
	var cont contributor
	L := language{}
	P.Name = "Pname"
	P.Owner.Name = "Powner"

	cont.Name = "Cname"
	cont.Contributions = 0
	C = append(C, cont)
	s := `{"Java": 10975078}`

	if err := json.Unmarshal([]byte(s), &L.languages); err != nil {
		panic(err)
	}

	realFP.Name = "Pname"
	realFP.Owner = "Powner"
	realFP.TopContributor = "Cname"
	realFP.Contributions = 0
	realFP.Languages = append(realFP.Languages, "Java")

	FP := makeFP(P, C, L)
	if FP.Name != realFP.Name {
		t.Error("FP.name not made properly")
	}

	if FP.Owner != realFP.Owner {
		t.Error("FP.Owner not made properly")
	}

	if FP.TopContributor != realFP.TopContributor {
		t.Error("FP.TopContributor not made properly")
	}

	if FP.Contributions != realFP.Contributions {
		t.Error("FP.Contributions not made properly")
	}

	if FP.Languages[0] != realFP.Languages[0] {
		t.Error("FP.Languages[0] not made properly")
	}
}
