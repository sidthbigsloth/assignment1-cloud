package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
)

type fullProject struct {
	Name           string   `json:"project"`
	Owner          string   `json:"owner"`
	TopContributor string   `json:"committer"`
	Contributions  int      `json:"commits"`
	Languages      []string `json:"language"`
}

type project struct {
	Name  string `json:"name"`
	Owner struct {
		Name string `json:"login"`
	} `json:"owner"`
}

type contributor struct {
	Name          string `json:"login"`
	Contributions int    `json:"contributions"`
}

type language struct {
	languages map[string]interface{}
}

func handler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	var FP fullProject
	var P project
	var C []contributor
	L := language{}
	http.Header.Add(w.Header(), "content-type", "application/json")

	url := makeBaseURL(r.URL.Path)
	if url == "" {
		status := 400
		http.Error(w, http.StatusText(status), status)
		return
	}
	page, err := client.Get(url)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewDecoder(page.Body).Decode(&P)
	page, err = client.Get(url + "/contributors")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewDecoder(page.Body).Decode(&C)
	page, err = client.Get(url + "/languages")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	body, err := ioutil.ReadAll(page.Body)

	if err := json.Unmarshal([]byte(body), &L.languages); err != nil {
		panic(err)
	}

	FP = makeFP(P, C, L)
	json.NewEncoder(w).Encode(FP)
}

func main() {
	http.HandleFunc("/projectinfo/v1/", handler)
	http.ListenAndServe(":8080", nil)

	appengine.Main()
}

func makeBaseURL(URL string) string {
	urlparts := strings.Split(URL, "/")
	if (len(urlparts) != 6 && len(urlparts) != 7) || urlparts[3] != "github.com" {
		return ""
	}
	return "https://api.github.com/repos/" + urlparts[4] + "/" + urlparts[5]
}

func makeFP(P project, C []contributor, L language) fullProject {
	var FP fullProject

	FP.Name = P.Name
	FP.Owner = P.Owner.Name

	FP.TopContributor = C[0].Name
	FP.Contributions = C[0].Contributions

	keys := reflect.ValueOf(L.languages).MapKeys()

	for _, elem := range keys {
		FP.Languages = append(FP.Languages, elem.Interface().(string))
	}

	return FP
}
